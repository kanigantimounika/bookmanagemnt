import React,{Suspense} from "react";

import { useEffect, useState } from "react";
import { Route, Switch, Redirect, withRouter } from "react-router-dom";
import Login from "./components/login/Login";
import Layout from "./components/layout/Layout";
import LoadingSpinner from "./components/UI/LoadingSpinner";

const ITEMS = [
	"The Hitchhiker's Guide to the Galaxy",
	"The Restaurant at the End of the Universe",
	"Life, the Universe and Everything",
	"So Long, and Thanks for All the Fish",
	"Mostly Harmless",
];

const Router = (props) => {


  const ItemList=React.lazy(()=>import('./components/items/ItemList'));
  const Cart=React.lazy(()=>import('./components/cart/Cart'));
  const PageNotFound=React.lazy(()=>import('./components/pages/PageNotFound'));
	const [cart, setCart] = useState([]);
	const [isAuthenticated, setIsAuthenticated] = useState(false);
  

	const handleLogin = (email, password) => {
		sessionStorage.setItem("email", email);
		sessionStorage.setItem("password", password);
    
		setIsAuthenticated(true);
		props.history.push("/items");
	};

	const handleLogout = () => {
		sessionStorage.removeItem("email");
		sessionStorage.removeItem("password");
    setCart([])
		setIsAuthenticated(false);
		props.history.push("/");
	};

	useEffect(() => {
		if (sessionStorage.getItem("email") && sessionStorage.getItem("password")) {
			setIsAuthenticated(true);
		}
	}, []);

	return (
		<Layout onLogout={handleLogout}  >
    <Suspense fallback={
      <div className='centered'>
        <LoadingSpinner />
      </div>
    }>
			<Switch>
				{!isAuthenticated && (
					<Route path="*">
						<Login onLogin={handleLogin} />
					</Route>
				)}
				<Redirect exact from="/" to="/items"></Redirect>
				<Route path="/items" exact>
					<ItemList items={ITEMS} cart={cart} setCart={setCart} />
				</Route>
        <Route path="/cart" exact>
					<Cart cart={cart} setCart={setCart} />
				</Route>
				<Route path="*">
					<PageNotFound />
				</Route>
			</Switch>
      </Suspense>
		</Layout>
	);
};

export default withRouter(Router);
