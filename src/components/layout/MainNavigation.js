import classes from './MainNavigation.module.css';
import { NavLink } from 'react-router-dom';
const MainNavigation=(props)=>{
   
    return <header className={classes.header}>
        {sessionStorage.getItem("email")===null ? <div className={classes.logo}>Login</div> : <div className={classes.logo}>Items</div> }
        {sessionStorage.getItem("email")!==null && 
        <nav className={classes.nav}>
            <ul>
                <li><NavLink activeClassName={classes.active} to="/items">Items</NavLink></li>
                <li><NavLink activeClassName={classes.active} to="/cart">Cart</NavLink></li>
                <li>
                     <span activeClassName={classes.active} onClick={props.onLogout}>Logout</span></li>
            </ul>
    
        </nav>}



    </header>

}
export default MainNavigation
/*
{!props.headerDisplay ?  <div className={classes.logo}>Login</div> :<div className={classes.logo}>Items</div>}
        {props.headerDisplay && */