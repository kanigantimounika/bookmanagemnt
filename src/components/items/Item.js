import classes from "./Item.module.css";

const Item = ({ name, isInCart, addToCart, removeFromCart }) => {
	return (
		<li className={classes.item}>
			<figure>
				<blockquote>
					<p>{name}</p>
				</blockquote>
			</figure>
			{!isInCart ? (
				<button onClick={() => addToCart(name)}>Add</button>
			) : (
				<button onClick={() => removeFromCart(name)}>Remove</button>
			)}
		</li>
	);
};

export default Item;
