import classes from "./BookList.module.css";
import {  Fragment,useMemo, useState } from "react";
import Item from "./Item";
const ItemList = ({ items, cart, setCart }) => {
	const [searchValue, setSearchValue] = useState("");

	const cartItems = useMemo(() => new Set(cart), [cart]);

	const handleAddToCart = (name) => {
		setCart([...cart, name]);
	};

	const handleRemoveFromCart = (name) => {
		setCart(cart.filter((item) => item !== name));
	};

	const handleSearch = (event) => {
		setSearchValue(event.target.value);
	};

	const itemList = useMemo(
		() =>
			items.filter((item) =>
				item.toLowerCase().includes(searchValue.toLowerCase())
			),
		[items, searchValue]
	);

	return (
		<Fragment>
			<input
				type="text"
				placeholder="Search"
				className={classes.search}
				value={searchValue}
				onChange={handleSearch}
			/>
			<ul className={classes.list}>
				{itemList.map((item) => {
					return (
						<Item
							key={item}
							name={item}
							isInCart={cartItems.has(item)}
							addToCart={handleAddToCart}
							removeFromCart={handleRemoveFromCart}
						/>
					);
				})}
			</ul>
		</Fragment>
	);
};
export default ItemList;

