import classes from "../items/BookList.module.css";
import Item from "../items/Item";

const Cart = ({ cart, setCart }) => {
   
	const handleAddToCart = (name) => {
		setCart([...cart, name]);
	};

	const handleRemoveFromCart = (name) => {
		setCart(cart.filter((item) => item !== name));
	};

	return (
		<ul className={classes.list}>
			{cart.map((item) => {
				return (
					<Item
						key={item}
						name={item}
						isInCart={true}
						addToCart={handleAddToCart}
						removeFromCart={handleRemoveFromCart}
					/>
				);
			})}
		</ul>
	);
};
export default Cart;
